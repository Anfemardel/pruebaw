<?php

namespace App\Http\Controllers;

use App\Persona;
use Illuminate\Http\Request;

class PersonaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $personas = Persona::get();
        echo json_encode($personas);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $persona = new Persona();
        $persona->nombres = $request->input('nombres');
        $persona->apellidos = $request->input('apellidos');
        $persona->direccion = $request->input('direccion');
        $persona->telefono = $request->input('telefono');
        $persona->email = $request->input('email');
        $persona->save();
        echo json_encode($persona);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Persona  $persona_id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $persona_id)
    {
        $persona = Persona::find($persona_id);
        $persona->nombres = $request->input('nombres');
        $persona->apellidos = $request->input('apellidos');
        $persona->direccion = $request->input('direccion');
        $persona->telefono = $request->input('telefono');
        $persona->email = $request->input('email');
        $persona->save();
        echo json_encode($persona);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Persona  $persona_id
     * @return \Illuminate\Http\Response
     */
    public function destroy($persona_id)
    {
        $persona = Persona::find($persona_id);
        $persona->delete();
    }
}
